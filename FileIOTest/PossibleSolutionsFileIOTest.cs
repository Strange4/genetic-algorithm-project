using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileIO;
using RobbyTheRobot;
using System;

namespace FileIOTest
{
    [TestClass]
    public class PossibleSolutionsFileIOTest
    {
        [TestMethod]
        [DataRow("NonExistentFolder")]
        [DataRow(@"TestFolders\NoValidFileName")]
        [DataRow(@"TestFolders\NotRightAmountOfCommas")]
        [DataRow(@"TestFolders\MaxScoreNotInt")]
        [DataRow(@"TestFolders\NbMovesNotInt")]
        [DataRow(@"TestFolders\GenomeNotInt")]
        [DataRow(@"TestFolders\GenomeNumsNotInRange")]
        [ExpectedException(typeof(ArgumentException))]
        public void GetGenomeResults_InvalidFolderOrFiles_ReturnsFalse(string folderPath)
        {
            GenomeResult[] grs = PossibleSolutionsFileIO.GetGenomeResults(folderPath);
        }

        [TestMethod]
        public void GetGenomeResults_ValidFolder_ReturnsTrueAndAppropriateGenomeResults()
        {
            GenomeResult[] grs = PossibleSolutionsFileIO.GetGenomeResults(@"TestFolders\Valid");

            Assert.AreEqual(1, grs[0].GenerationNum);
            Assert.AreEqual(500, grs[0].MaxScore);
            Assert.AreEqual(10, grs[0].NumMoves);
            Assert.AreEqual(2, grs[1].GenerationNum);
            Assert.AreEqual(400, grs[1].MaxScore);
            Assert.AreEqual(4, grs[1].NumMoves);
            Assert.AreEqual((int)PossibleMoves.South, grs[0].MovesToTake[0]);
            Assert.AreEqual((int)PossibleMoves.East, grs[0].MovesToTake[1]);
            Assert.AreEqual((int)PossibleMoves.Nothing, grs[0].MovesToTake[2]);
            Assert.AreEqual((int)PossibleMoves.Random, grs[1].MovesToTake[0]);
            Assert.AreEqual((int)PossibleMoves.Random, grs[1].MovesToTake[1]);
            Assert.AreEqual((int)PossibleMoves.Random, grs[1].MovesToTake[2]);
        }

        [TestMethod]
        public void SaveGenomeResultsTest()
        {
            GenomeResult[] grs1 = PossibleSolutionsFileIO.GetGenomeResults(@"TestFolders\Valid");
            PossibleSolutionsFileIO.SaveGenomeResults(@"TestFolders\TempDir", grs1);
            GenomeResult[] grs2 = PossibleSolutionsFileIO.GetGenomeResults(@"TestFolders\TempDir");

            Assert.AreEqual(grs2[0].GenerationNum, grs1[0].GenerationNum);
            Assert.AreEqual(grs2[0].MaxScore, grs1[0].MaxScore);
            Assert.AreEqual(grs2[0].NumMoves, grs1[0].NumMoves);
            Assert.AreEqual(grs2[0].GenerationNum, grs1[0].GenerationNum);
            Assert.AreEqual(grs2[0].MaxScore, grs1[0].MaxScore);
            Assert.AreEqual(grs2[0].NumMoves, grs1[0].NumMoves);
            Assert.AreEqual(grs2[0].MovesToTake[0], grs1[0].MovesToTake[0]);
            Assert.AreEqual(grs2[0].MovesToTake[1], grs1[0].MovesToTake[1]);
            Assert.AreEqual(grs2[0].MovesToTake[2], grs1[0].MovesToTake[2]);
            Assert.AreEqual(grs2[0].MovesToTake[0], grs1[0].MovesToTake[0]);
            Assert.AreEqual(grs2[0].MovesToTake[1], grs1[0].MovesToTake[1]);
            Assert.AreEqual(grs2[0].MovesToTake[2], grs1[0].MovesToTake[2]);
        }
    }
}
