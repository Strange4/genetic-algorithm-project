using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileIO;
using RobbyTheRobot;

namespace FileIOTest
{
    [TestClass]
    public class GenomeResultTest
    {
        [TestMethod]
        public void GenerationNum_Construtor_ReturnsSameNumber()
        {
            GenomeResult gr = new GenomeResult(10, 1, 2, new int[3]);

            Assert.AreEqual(10, gr.GenerationNum);
        }
        [TestMethod]
        public void MaxScore_Construtor_ReturnsSameNumber()
        {
            GenomeResult gr = new GenomeResult(1, 1, 2, new int[3]);

            Assert.AreEqual(1, gr.MaxScore);
        }

        [TestMethod]
        public void NumMoves_Construtor_ReturnsSameNumber()
        {
            GenomeResult gr = new GenomeResult(1, 1, 2, new int[3]);

            Assert.AreEqual(2, gr.NumMoves);
        }

        [TestMethod]
        public void MovesToTake_Construtor_ReturnsCopyOfArray()
        {
            int[] pm = new int[3];
            pm[0] = (int)PossibleMoves.East;
            pm[1] = (int)PossibleMoves.North;
            pm[2] = (int)PossibleMoves.South;
            GenomeResult gr = new GenomeResult(1, 1, 2, pm);

            int[] actualPm = gr.MovesToTake;

            Assert.AreEqual((int)PossibleMoves.East, actualPm[0]);
            Assert.AreEqual((int)PossibleMoves.North, actualPm[1]);
            Assert.AreEqual((int)PossibleMoves.South, actualPm[2]);
        }
    }
}
