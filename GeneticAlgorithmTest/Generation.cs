using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticAlgorithm;
namespace GeneticAlgorithmTest
{
    [TestClass]
    public class GenerationTests
    {
        [TestMethod]
        public void EvaluateFitness_WithFitnessHandler_ShouldBeInOrder()
        {
            var counter = 1;
            var gAlgo = new GeneticAlgorithm.GeneticAlgorithm(200,243,7, .1, .1, 3, (chromosome, generation)=>counter++, 1234);
            var generation = new Generation(gAlgo);
            generation.EvaluateFitnessOfPopulation();
            for(int i=0;i<generation.NumberOfChromosomes - 1;i++)
            {
                Assert.IsTrue(generation[i].Fitness > generation[i+1].Fitness);
            }
        }
    }
}