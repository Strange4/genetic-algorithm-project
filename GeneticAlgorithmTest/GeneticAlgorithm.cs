using Microsoft.VisualStudio.TestTools.UnitTesting;

using GeneticAlgorithm;

namespace GeneticAlgorithmTest
{
    [TestClass]
    public class GeneticAlgorithmTests
    {
        [TestMethod]
        public void GenerateGeneration_WithoutSeed_HaveTheSamePopulationSize()
        {
            var gAlgo = new GeneticAlgorithm.GeneticAlgorithm(200,243,7, .1, .1, 3, (chromosome, generation)=>10, 1234);
            gAlgo.GenerateGeneration();
            Assert.AreEqual(200, gAlgo.CurrentGeneration.NumberOfChromosomes);
        }
    }

    
}