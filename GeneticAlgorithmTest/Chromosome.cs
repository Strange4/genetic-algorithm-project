using Microsoft.VisualStudio.TestTools.UnitTesting;

using GeneticAlgorithm;
using System.Linq;

namespace GeneticAlgorithmTest
{
    [TestClass]
    public class ChromosomeTests
    {

        [TestMethod]
        public void Constructor_WithSeed_AreRandomGenes()
        {
            var c = new Chromosome(10, 7);
            foreach (var g in c.Genes)
            {
                Assert.IsTrue(g >= 0 && g <= 7);
            }
        }

        [TestMethod]
        // testing that the reproduction is reproducible
        public void Reproduce_WithSeed_AreExpectedToCrossOver()
        {
            var c = new Chromosome(10, 7, 1234);
            var c2 = new Chromosome(10, 7, 2345);
            var children = c.Reproduce(c2, 0);
            var child1 = children[0];
            var child2 = children[1];

            var expectedPoint1 = 3;
            var expectedPoint2 = 8;
            for (int i = expectedPoint1; i < (expectedPoint2 - expectedPoint1); i++)
            {
                Assert.AreEqual(c[i], child2[i]);
                Assert.AreEqual(c2[i], child1[i]);
            }
        }
    }
}
