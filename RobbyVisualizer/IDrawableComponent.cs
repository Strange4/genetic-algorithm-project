using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{

  public interface IDrawableComponent
  {
    /// <summary>
    /// Do a draw.
    /// </summary>
    /// <param name="gameTime">game time recived form the Drawable game component</param>
    /// <param name="spriteBatch">sprite batch recived form the Drawable game component</param>
    public void Draw(GameTime gameTime, SpriteBatch spriteBatch);
  }
}