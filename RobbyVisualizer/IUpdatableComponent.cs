using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{
  public interface IUpdatableComponent
  {
    /// <summary>
    /// Do an update.
    /// </summary>
    /// <param name="gameTime">game time recived form the Drawable game component</param>
    public void Update(GameTime gameTime);
  }
}