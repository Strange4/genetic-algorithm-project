using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace RobbyVisualizer
{
  /// <summary>
  /// Used to display text
  /// </summary>
  public class TextDisplay : IDrawableComponent
  {

    #region Fields
    private static readonly int Padding = 15;
    private SpriteFont _font;
    private string _text;

    #endregion

    #region Properties

    public Texture2D Texture {get; set;}

    public Vector2 Position {get; set;}
    public bool Show {get; set;}

    public event EventHandler ChangeText;

    /// <summary>
    /// Represents the position and dimensions of the text box.
    /// </summary>
    public Rectangle Rectangle 
    { 
      get
      {
        if(Text == null) Text = "";
        return new Rectangle(
          (int)Position.X, 
          (int)Position.Y, 
          (int)_font.MeasureString(Text).X + 2 * Padding, 
          (int)_font.MeasureString(Text).Y + 2 * Padding
        );
      } 
    }

    public virtual string Text 
    {
      get
      {
        return _text;
      } 
      set
      {
        _text = value;
        ChangeText?.Invoke(this, new EventArgs());
      }
    }
    public Color TextColor {get; set;}
    public Color BackgroundColor {get; set;}

    #endregion

    #region Methods

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="font">Font used to display text</param>
    public TextDisplay(SpriteFont font)
    {
      if(font == null)
        throw new ArgumentException();
      _font = font;

      Position = new Vector2(0,0);
      TextColor = Color.Black;
      BackgroundColor = Color.White;
      Text = "";
      Show = true;
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      if(Show)
      {
        if(Texture != null)
          spriteBatch.Draw(Texture, Rectangle, BackgroundColor);

        // spriteBatch.Draw(_texture, Position,
        //   null, color, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);

        if(!string.IsNullOrEmpty(Text)){

          float x = (Rectangle.X + (Rectangle.Width / 2)) - (_font.MeasureString(Text).X / 2);
          float y = (Rectangle.Y + (Rectangle.Height / 2)) - (_font.MeasureString(Text).Y / 2);
          spriteBatch.DrawString(_font, Text, new Vector2(x, y), TextColor);
        }
      }
      
    }

    #endregion

    
  }
}

