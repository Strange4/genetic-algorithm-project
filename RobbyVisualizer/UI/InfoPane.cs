using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{
  /// <summary>
  /// Displays Score, Move number, and Generation number
  /// </summary>
  public class InfoPane : IDrawableComponent
  {

    #region Fields

    private static readonly int Padding = 30;
    private static readonly int ProgressBarHeight = 40;
    private NumberDisplay _generationDisplay;
    private ProgressionBar _scoreProgress;
    private ProgressionBar _movesProgress;
    private List<IDrawableComponent> _drawableComponents;
    private Texture2D _texture;
    private SpriteFont _font;

    #endregion

    #region Properties
    public bool Show{ get; set;}
    public int MaxScore{ set { _scoreProgress.MaxNum = value; }}
    public int MaxMoves{ set { _movesProgress.MaxNum = value; }}
    public int CurrentScore{ get {return _scoreProgress.CurrentNum;} set { _scoreProgress.CurrentNum = value; }}
    public int CurrentMove{ get { return _movesProgress.CurrentNum; } set { _movesProgress.CurrentNum = value; }}
    public int GenerationNum{ set { _generationDisplay.Num = value; }}

    public Rectangle Transform {get; private set;}

    #endregion

    #region Methods

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="transform">Rectangle that represents the position and size of the info pane</param>
    /// <param name="texture">generic white square used in different components</param>
    /// <param name="font">Font used to display text</param>
    /// <param name="maxScore">Maximum possible score</param>
    /// <param name="nbMoves">number of moves</param>
    public InfoPane(Rectangle transform, Texture2D texture, SpriteFont font, int maxScore=10, int nbMoves=10)
    {
      Show = true;
      Transform = transform;
      _texture = texture;
      _font = font;
      _movesProgress = new ProgressionBar(
        new Rectangle(Padding, Padding, Transform.Width/3, ProgressBarHeight), 
        nbMoves, 
        _texture,
        _font
      ){
        Text = "Moves:"
      };
      _scoreProgress = new ProgressionBar(
        new Rectangle(
          Padding + _movesProgress.BackgroundTransform.Width + _movesProgress.BackgroundTransform.X, 
          Padding, Transform.Width/3, ProgressBarHeight), 
        maxScore, 
        _texture,
        _font
      ){
        Text = "Score:"
      };
      _generationDisplay = new NumberDisplay(_font){
        Title = "Generation:",
        Texture = _texture
      };
      // this is updated once before the number was added
      _generationDisplay.Position = new Vector2(Transform.Width - _generationDisplay.Rectangle.Width - Padding, Padding);
      _generationDisplay.ChangeText += (sender, e) => _generationDisplay.Position = new Vector2(Transform.Width - _generationDisplay.Rectangle.Width - Padding, Padding);

      _drawableComponents = new List<IDrawableComponent>{
        _movesProgress,
        _scoreProgress,
        _generationDisplay
      };
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      if(Show)
      {
        foreach (var component in _drawableComponents)
        {
          component.Draw(gameTime, spriteBatch);
        }
      }
      
    }

    #endregion

    
  }
}

