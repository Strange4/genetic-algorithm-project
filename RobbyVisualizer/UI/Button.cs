using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{
  /// <summary>
  /// Monogame clickable button object
  /// followed tutorial: https://www.youtube.com/watch?v=lcrgj26G5Hg&ab_channel=Oyyou
  /// </summary>
  public class Button : IDrawableComponent, IUpdatableComponent
  {

    #region Fields
    private MouseState _currentMouse;

    private MouseState _previousMouse;

    private bool _isHover;

    private SpriteFont _font;

    private Texture2D _texture;

    #endregion

    #region Properties

    public event EventHandler Click;

    public Vector2 Position {get; set;}
    public bool Show {get; set;}

    /// <summary>
    /// Rectangle to store the size and position of the button
    /// </summary>
    public Rectangle Rectangle 
    { 
      get
      {
        return new Rectangle((int)Position.X, (int)Position.Y, _texture.Width, _texture.Height);
      } 
    }
    
    public string Text {get; set;}
    public Color HoverColor {get; set;}
    public Color TextColor {get; set;}

    #endregion

    #region Methods

    public Button(Texture2D texture, SpriteFont font)
    {
      if(texture == null || font == null)
        throw new ArgumentException();
      _texture = texture;
      _font = font;

      Position = new Vector2(0,0);
      HoverColor = Color.Gray;
      TextColor = Color.Black;
      Show = true;
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      if(Show)
      {
        Color color = Color.White;
        if(_isHover)
          color = HoverColor;

        spriteBatch.Draw(_texture, Position, color);

        // spriteBatch.Draw(_texture, Position,
        //   null, color, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);
        if(!string.IsNullOrEmpty(Text)){

          float x = (Rectangle.X + (Rectangle.Width / 2)) - (_font.MeasureString(Text).X / 2);
          float y = (Rectangle.Y + (Rectangle.Height / 2)) - (_font.MeasureString(Text).Y / 2);
          spriteBatch.DrawString(_font, Text, new Vector2(x, y), TextColor);
        }
      }
    }
    public void Update(GameTime gameTime)
    {
      if(Show)
      {
        _previousMouse = _currentMouse;
        _currentMouse = Mouse.GetState();
        Rectangle mouseRect = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);

        _isHover = mouseRect.Intersects(Rectangle);

        if(_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed && _isHover)
        {
          //The EventArgs class is the base type for all event data classes. EventArgs is also the class you use when an event doesn't have any data associated with it. - MS docs
          Click?.Invoke(this, new EventArgs());
        }
      }
    }

    #endregion

    
  }
}

