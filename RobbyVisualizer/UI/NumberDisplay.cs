using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{
  public class NumberDisplay : TextDisplay
  {

    #region Fields
    private string _title;
    private int _num;

    #endregion

    #region Properties
    public new event EventHandler ChangeText;

    /// <summary>
    /// returns the tile (prefix) concatinated with the number
    /// </summary>
    public override string Text 
    { 
      get
      {
        string displayText = Num.ToString();
        if(!string.IsNullOrEmpty(Title))
          displayText = Title + " " + displayText;
        return displayText;
      }
      
      set
      {
        Title = value;
        ChangeText?.Invoke(this, new EventArgs());
      }
    }

    // make sure to call change text when modif num 
    public string Title
    {
      get => _title;
      set{ _title = value; ChangeText?.Invoke(this, new EventArgs());}
    }
    public int Num
    {
      get => _num;
      set{ _num = value; ChangeText?.Invoke(this, new EventArgs());}
    }

    #endregion

    #region Methods

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="font">Font used to display text</param>
    public NumberDisplay(SpriteFont font) : base(font)
    {
      if(font == null)
        throw new ArgumentException();

      Num = 0;
    }

    #endregion

    
  }
}

