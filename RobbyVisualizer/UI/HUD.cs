using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using FileIO;
using System.Windows.Forms;

namespace RobbyVisualizer
{
  /// <summary>
  /// Represents the Heads Up Display UI that provides extra information to the user and buttons
  /// </summary>
  public class HUD : IDrawableComponent, IUpdatableComponent
  {
    private static readonly int BtnMargin = 30;
    private Game _game;
    private List<IDrawableComponent> _drawableComponents;
    private List<IUpdatableComponent> _updatableComponents;
    private Texture2D _smallBtnTexture;
    private Texture2D _btnTexture;
    private Texture2D _whiteSquare;
    private SpriteFont _font;
    private InfoPane _infoPane;
    private TextDisplay _messageDisplay;
    private Button _getGenomesBtn;
    private Button _messageXBtn;
    private Button _quitBtn;
    public Button RobbySpeedBtn {get; set;}

    private GraphicsDevice _graphicsDevice;

    public InfoPane InfoPane => _infoPane;
    public event Action<GenomeResult[]> GetGenomeResults;
    
    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="game">game</param>
    /// <param name="graphicsDevice">graphics device</param>
    /// <param name="smallBtnTexture">Small Button texture used to close a message</param>
    /// <param name="btnTexture">Button texture</param>
    /// <param name="whiteSquare">generic white square used in different components</param>
    /// <param name="font">Font used to display text</param>
    public HUD(
      Game game, 
      GraphicsDevice graphicsDevice,
      Texture2D smallBtnTexture,
      Texture2D btnTexture,
      Texture2D whiteSquare,
      SpriteFont font
    )
    {
      _game = game;
      _graphicsDevice = graphicsDevice;
      _smallBtnTexture = smallBtnTexture;
      _btnTexture = btnTexture;
      _font = font;
      _whiteSquare = whiteSquare;
      InitMessageDisplay();
      InitInfoPane();
      InitButtons();

      _drawableComponents = new List<IDrawableComponent>{
        _getGenomesBtn,
        _quitBtn,
        _infoPane,
        _messageDisplay,
        _messageXBtn,
        RobbySpeedBtn
      };

      _updatableComponents = new List<IUpdatableComponent>{
        _getGenomesBtn,
        _quitBtn,
        _messageXBtn,
        RobbySpeedBtn
      };
    }

    /// <summary>
    /// Initializes the message displaying functionality
    /// </summary>
    private void InitMessageDisplay()
    {
      _messageDisplay = new TextDisplay(_font)
      {
        Show = false
      };
      _messageDisplay.Texture = _whiteSquare;
      _messageXBtn = new Button(_smallBtnTexture, _font)
      {
        Text = "x",
        HoverColor = Color.Red,
        Show = false
      };
      _messageDisplay.ChangeText += (sender, e) =>
      {
        float messageX = (_graphicsDevice.Viewport.Width / 2) - (_messageDisplay.Rectangle.Width / 2);
        float messageY = (_graphicsDevice.Viewport.Height * (3f / 4f)) - (_messageDisplay.Rectangle.Height / 2);
        _messageDisplay.Position = new Vector2(messageX, messageY);
        _messageXBtn.Position = new Vector2(
          _messageDisplay.Position.X + _messageDisplay.Rectangle.Width - (_messageXBtn.Rectangle.Width / 2),
          _messageDisplay.Position.Y - (_messageXBtn.Rectangle.Height / 2)
        );
      };
    }

    /// <summary>
    /// Shows a message to the user.
    /// </summary>
    /// <param name="message">message string</param>
    /// <param name="textColor">Color of the text, if null: black</param>
    /// <param name="bgColor">Background color, if null: no background</param>
    public void ShowMessage(String message, Color textColor, Color bgColor)
    {
      _messageDisplay.Text = message;
      if(textColor != null)
        _messageDisplay.TextColor = textColor;
      if(bgColor != null)
      {
        _messageDisplay.Texture = _whiteSquare;
        _messageDisplay.BackgroundColor = bgColor;
      } 
      else
        _messageDisplay.Texture = null;

      _messageDisplay.Show = true;
      _messageXBtn.Show = true;

      _messageXBtn.Click += (sender, e) => {
        _messageDisplay.Show = false;
        _messageXBtn.Show = false;
      };

    }

    /// <summary>
    /// Initializes the quit and get genome results button
    /// </summary>
    private void InitButtons()
    {
      _getGenomesBtn = new Button(_btnTexture, _font)
      {
        Text = "Get Genomes"
      };
      _getGenomesBtn.Position = new Vector2(_graphicsDevice.Viewport.Width - _getGenomesBtn.Rectangle.Width - BtnMargin, _infoPane.Transform.Height + BtnMargin);

      _getGenomesBtn.Click += LoadGenomeResults;

      _quitBtn = new Button(_btnTexture, _font)
      {
        Text = "Quit",
      };
      _quitBtn.Position = new Vector2(_graphicsDevice.Viewport.Width - _quitBtn.Rectangle.Width - BtnMargin, _getGenomesBtn.Position.Y + _getGenomesBtn.Rectangle.Height + BtnMargin);

      _quitBtn.Click += (sender, e) => _game.Exit();

      RobbySpeedBtn = new Button(_btnTexture, _font){
        Show = false
      };
    }

    /// <summary>
    /// Initializes the infopane
    /// </summary>
    private void InitInfoPane()
    {
      _infoPane = new InfoPane(new Rectangle(0, 0, _graphicsDevice.Viewport.Width, _graphicsDevice.Viewport.Height / 6), _whiteSquare, _font)
      {
        CurrentMove = 0,
        CurrentScore = 0,
        GenerationNum = 0,
        Show = true
      };
    }

    /// <summary>
    /// Callback to an event that loads the genome results using FolderBrowserDialog
    /// Calls the GetGenomeResults event
    /// </summary>
    private void LoadGenomeResults(object sender, System.EventArgs e)
    {

      var folderBrowserDialog = new FolderBrowserDialog();
      // Set the help text description for the FolderBrowserDialog.
      folderBrowserDialog.Description = 
          "Select the directory with your Genome Results.";

      // Do not allow the user to create new files via the FolderBrowserDialog.
      folderBrowserDialog.ShowNewFolderButton = false;

      // Default to the My Documents folder.
      folderBrowserDialog.RootFolder = Environment.SpecialFolder.Personal;

      if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
      {
        string folderPath = folderBrowserDialog.SelectedPath;
        try
        {
          GenomeResult[] gr = PossibleSolutionsFileIO.GetGenomeResults(folderPath);
          GetGenomeResults?.Invoke(gr);
        }
        catch (Exception exception)
        {
          ShowMessage(exception.Message, Color.White, Color.Red);
        }
      }
      else
      {
        ShowMessage("Not able to get Genome Results", Color.White, Color.Red);
      }
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      foreach (var component in _drawableComponents)
      {
        component.Draw(gameTime, spriteBatch);
      }
    }

    public void Update(GameTime gameTime)
    {
      foreach (var component in _updatableComponents)
      {
        component.Update(gameTime);
      }
    }
  }
}