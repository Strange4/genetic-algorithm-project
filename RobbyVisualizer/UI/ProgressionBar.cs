using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace RobbyVisualizer
{
  /// <summary>
  /// Progress bar, can be used to represent loading or a health bar, etc...
  /// </summary>
  public class ProgressionBar : IDrawableComponent
  {

    #region Fields

    private static readonly int Padding = 10;
    private SpriteFont _font;

    private Texture2D _texture;

    #endregion

    #region Properties

    public Rectangle BackgroundTransform {get; private set;}

    /// <summary>
    /// rectangle representing the size and position of the progression part of the bar
    /// </summary>
    public Rectangle ProgressTransform 
    {
      get
      {
        return new Rectangle(
          (int)BackgroundTransform.X + Padding, 
          (int)BackgroundTransform.Y + Padding, 
          (int)(Math.Min(Math.Abs((float)CurrentNum/(float)MaxNum), 1f) * (BackgroundTransform.Width - 2 * Padding)),
          BackgroundTransform.Height - 2 * Padding
        );
      }
    }
    
    public string Text {get; set;}
    public Color TextColor {get; set;}
    public Color BackgroundColor {get; set;}
    public Color ProgressColor {get; set;}
    public Color NegativeProgressColor {get; set;}
    public int CurrentNum {get; set;}
    public int MaxNum {get; set;}

    #endregion

    #region Methods

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="transform">rectangle representing the size and position of the progression bar</param>
    /// <param name="maxNum">maximum number that can be reached</param>
    /// <param name="texture">generic white square used in different components</param>
    /// <param name="font">Font used to display text</param>
    public ProgressionBar(Rectangle transform, int maxNum, Texture2D texture, SpriteFont font)
    {
      if(transform == null || maxNum == 0 || texture == null || font == null)
        throw new ArgumentException();
      _texture = texture;
      _font = font;

      MaxNum = maxNum;
      BackgroundTransform = transform;
      TextColor = Color.Black;
      BackgroundColor = Color.DarkBlue;
      ProgressColor = Color.Red;
      NegativeProgressColor = Color.Purple;
      
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      spriteBatch.Draw(_texture, BackgroundTransform, BackgroundColor);

      Color finalProgressColor = ProgressColor;
      if(CurrentNum < 0)
        finalProgressColor = NegativeProgressColor;

      spriteBatch.Draw(_texture, ProgressTransform, finalProgressColor);

      // spriteBatch.Draw(_texture, Position,
      //   null, color, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.1f);

      string displayText = CurrentNum + "/" + MaxNum;
      if(!string.IsNullOrEmpty(Text))
        displayText = Text + " " + displayText;

      float x = BackgroundTransform.X + BackgroundTransform.Width  - _font.MeasureString(displayText).X;
      float y = BackgroundTransform.Y + BackgroundTransform.Height + Padding;
      spriteBatch.DrawString(_font, displayText, new Vector2(x, y), TextColor);
    }

    #endregion

    
  }
}

