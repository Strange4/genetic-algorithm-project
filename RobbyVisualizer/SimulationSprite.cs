using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace RobbyVisualizer
{
  public class SimulationSprite : DrawableGameComponent
  {
    private Game _game;
    private List<IDrawableComponent> _drawableComponents;
    private List<IUpdatableComponent> _updatableComponents;
    private SpriteBatch _spriteBatch;
    private HUD _hud;
    private SimulationManager _simulationManager;
    
    public SimulationSprite(Game game) : base(game)
    {
      _game = game;
    }

    public override void Initialize()
    {      
      base.Initialize();
    }

    protected override void LoadContent()
    {
      _spriteBatch = new SpriteBatch(GraphicsDevice);

      _hud = new HUD(
        _game,
        GraphicsDevice,
        _game.Content.Load<Texture2D>("UI/button-small"),
        _game.Content.Load<Texture2D>("UI/button"),
        _game.Content.Load<Texture2D>("white-square"),
        _game.Content.Load<SpriteFont>("Fonts/Font")
      );

      _hud.InfoPane.Show = false;


      SoundEffect pickupSound = null;
      try
      {
        pickupSound =_game.Content.Load<SoundEffect>("Simulation/pickup-generic");
      }
      catch (NoAudioHardwareException)
      {
        
      }

      _simulationManager = new SimulationManager(
        GraphicsDevice,
        _game.Content.Load<Texture2D>("Simulation/grid-block"),
        _game.Content.Load<Texture2D>("Simulation/soda-can"),
        _game.Content.Load<Texture2D>("Simulation/robby-robot"),
        _game.Content.Load<Texture2D>("Simulation/robby-robot-right"),
        pickupSound,
        _hud
      );

      _hud.GetGenomeResults += _simulationManager.StartSimulation;

      _drawableComponents = new List<IDrawableComponent>(){
        _simulationManager,
        _hud
      };
      _updatableComponents = new List<IUpdatableComponent>(){
        _simulationManager,
        _hud
      };
      
      base.LoadContent();
    }

    public override void Draw(GameTime gameTime)
    {
      // _spriteBatch.Begin(SpriteSortMode.FrontToBack); you can use this to layer draws
      _spriteBatch.Begin();
      foreach (var component in _drawableComponents)
      {
        component.Draw(gameTime, _spriteBatch);
      }
      _spriteBatch.End();
      base.Draw(gameTime);
    }

    public override void Update(GameTime gameTime)
    {
      foreach (var component in _updatableComponents)
      {
        component.Update(gameTime);
      }
      base.Update(gameTime);
    }

  }
}