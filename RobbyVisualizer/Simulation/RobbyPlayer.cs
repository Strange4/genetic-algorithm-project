using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System;
using RobbyTheRobot;

namespace RobbyVisualizer
{
  
  /// <summary>
  /// Represents robby the robot
  /// </summary>
  public class RobbyPlayer : IDrawableComponent, IUpdatableComponent
  {
    private Random _rng; 
    private Texture2D _robbyRobotLeftTexture;
    private Texture2D _robbyRobotRightTexture;
    private SoundEffect _pickupSound;

    public bool IsMovingLeft {get; private set;}

    public Texture2D CurrentTexture => IsMovingLeft ? _robbyRobotLeftTexture : _robbyRobotRightTexture;

    public int Height => CurrentTexture.Height;
    public int Width => CurrentTexture.Width;
    public int CurrMove {get; set;}
    public int MaxMoves {get; set;}
    // is nullable because at the start we dont know the position of robby
    public Vector2? Position {get; set;}
    public Vector2 RobbyCoordinates {get; set;}
    public ContentsOfGrid[,] GridContents {get; set;}    
    public double MovementInterval {get; set;}
    private double ElapsedTime {get; set;}
    public int[] Genome {get; set;}

    public event Action<double> OnScore;
    public event EventHandler OnMovesFinished;

    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="robbyRobotTexture">Robby's texture</param>
    /// <param name="genome">genome</param>
    public RobbyPlayer(Texture2D robbyRobotLeftTexture, Texture2D robbyRobotRightTexture, SoundEffect pickupSound, int[] genome)
    { 
      _pickupSound = pickupSound;
      _rng = new Random();
      _robbyRobotLeftTexture = robbyRobotLeftTexture;
      _robbyRobotRightTexture = robbyRobotRightTexture;
      Genome = genome;
      RobbyCoordinates = new Vector2(0,0);
      Position = null;
      CurrMove = 1;

      MovementInterval = 1000;
    }

    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
      if(Position != null)
      {
        spriteBatch.Draw(CurrentTexture, (Vector2)Position, Color.White);
      }
    }

    public void Update(GameTime gameTime)
    {
      if(GridContents != null && CurrMove <= MaxMoves)
      {
        ElapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;
        if(ElapsedTime > MovementInterval)
        {
          ElapsedTime -= MovementInterval;

          int x = (int)RobbyCoordinates.X;
          int y = (int)RobbyCoordinates.Y;
          // the grid will automaticaly update because its a reference type
          double score = RobbyHelper.ScoreForAllele(Genome, GridContents, _rng, ref x, ref y);

          Vector2 oldPosition = RobbyCoordinates;
          Vector2 newPosition = new Vector2(x, y);

          if(oldPosition.X != newPosition.X || oldPosition.Y != newPosition.Y)
            IsMovingLeft = oldPosition.X > newPosition.X ||  oldPosition.Y < newPosition.Y;

          RobbyCoordinates = newPosition;

          if(score == 10) _pickupSound?.Play();

          OnScore?.Invoke(score);
          CurrMove++;

          if(CurrMove >= MaxMoves)
          {
            OnMovesFinished?.Invoke(this, new EventArgs());
          }
        }

      }
    }
  }

}