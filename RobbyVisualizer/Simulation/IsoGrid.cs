using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using FileIO;
using System.Windows.Forms;
using RobbyTheRobot;

namespace RobbyVisualizer
{
  
  /// <summary>
  /// Represents an Isometric grid
  /// draws robby, the cans and the grid
  /// Robby’s world consists of 100 squares (sites) laid out in a 10 × 10 grid. 
  /// tutorial for isometric: https://www.youtube.com/watch?v=04oQ2jOUjkU&ab_channel=JordanWest
  /// </summary>
  public class IsoGrid : IDrawableComponent, IUpdatableComponent
  {
    private static readonly int RobbyCanMargin = 6;
    public RobbyPlayer RobbyPlayer {get; set;}
    private Texture2D _gridBlockTexture;
    private Texture2D _sodaCanTexture;
    public float Height => _gridBlockTexture.Height/2 * GridContents.GetLength(0);
    public float Width => _gridBlockTexture.Width * GridContents.GetLength(1);
    public Vector2 Position {get; set;}

    public ContentsOfGrid[,] GridContents {get; set;}

    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="gridBlockTexture">texture for a single grid block</param>
    /// <param name="sodaCanTexture">Texture of the soda can</param>
    /// <param name="gridContents">array of grid contents</param>
    public IsoGrid(Texture2D gridBlockTexture, Texture2D sodaCanTexture, ContentsOfGrid[,] gridContents)
    {
      _gridBlockTexture = gridBlockTexture;
      _sodaCanTexture = sodaCanTexture;
      Position = new Vector2(0,0);
      GridContents = gridContents;
    }
    
    public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
     
      for (int x = 0; x < GridContents.GetLength(1); x++)
      {
        for (int y = 0; y < GridContents.GetLength(0); y++)
        {
          spriteBatch.Draw(_gridBlockTexture, GridCoordinateToScreenPosition(x, y), Color.White);

          if(GridContents[x,y] == ContentsOfGrid.Can){
            
            int xBias = 0;
            if(RobbyPlayer?.RobbyCoordinates == new Vector2(x,y))
              if(RobbyPlayer.IsMovingLeft)
                xBias = - (_sodaCanTexture.Width/2 + RobbyCanMargin*2);
              else
                xBias = _sodaCanTexture.Width/2 + RobbyCanMargin*2;


            spriteBatch.Draw(_sodaCanTexture, PositionOnTopBlock(x, y, _sodaCanTexture.Width, _sodaCanTexture.Height, xBias), Color.White);
          }
        }
      }

      RobbyPlayer?.Draw(gameTime, spriteBatch);
    }

    /// <summary>
    /// gets screen position based on grid coordinates
    /// </summary>
    /// <param name="x">x coordinate</param>
    /// <param name="y">y coordinate</param>
    /// <returns>screen position</returns>
    public Vector2 GridCoordinateToScreenPosition(int x, int y)
    {
      var i = new Vector2(.5f * _gridBlockTexture.Width, .25f * _gridBlockTexture.Height) * x;
      var j = new Vector2(-.5f * _gridBlockTexture.Width, .25f * _gridBlockTexture.Height) * y;
      var sum = i + j + Position;
      sum.X = sum.X - _gridBlockTexture.Width / 2;
      return sum;
    }

    /// <summary>
    /// gets screen position based on grid coordinates for object that is placed on the grid (like a can or robby)
    /// </summary>
    /// <param name="x">x coordinate</param>
    /// <param name="y">y coordinate</param>
    /// <param name="objectWidth">height of the object</param>
    /// <param name="objectHeight">height of the object</param>
    /// <returns>screen position</returns>
    public Vector2 PositionOnTopBlock(int x, int y, int objectWidth, int objectHeight, int xBias = 0)
    {
      Vector2 blockPosition = GridCoordinateToScreenPosition(x, y);
      return new Vector2(
        blockPosition.X + _gridBlockTexture.Width/2 - objectWidth/2 + xBias,
        blockPosition.Y + _gridBlockTexture.Height/4 - objectHeight + objectHeight/8
      );
    }

    public void Update(GameTime gameTime)
    {
      if(RobbyPlayer != null)
      {
        RobbyPlayer.GridContents = GridContents;
        RobbyPlayer.Update(gameTime);
        GridContents = RobbyPlayer.GridContents;
        int x = (int)RobbyPlayer.RobbyCoordinates.X;
        int y = (int)RobbyPlayer.RobbyCoordinates.Y;
        int xBias = 0;
        if(GridContents[x,y] == ContentsOfGrid.Can)
          if(RobbyPlayer.IsMovingLeft)
            xBias = RobbyPlayer.Width/2 - RobbyCanMargin;
          else
            xBias = - (RobbyPlayer.Width/2 - RobbyCanMargin);

        RobbyPlayer.Position = PositionOnTopBlock(
          x, 
          y,
          RobbyPlayer.Width, 
          RobbyPlayer.Height,
          xBias
        );
      }
    }
  }

}