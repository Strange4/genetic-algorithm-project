using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using System;
using FileIO;
using RobbyTheRobot;

namespace RobbyVisualizer
{

    /// <summary>
    /// Manages everything to do with the simulation
    /// </summary>
    public class SimulationManager : IDrawableComponent, IUpdatableComponent
    {
        private readonly int GridSize = 10;
        private Random _rand;
        private Texture2D _robbyRobotLeftTexture;
        private Texture2D _robbyRobotRightTexture;
        private SoundEffect _pickupSound;
        private List<IDrawableComponent> _drawableComponents;
        private List<IUpdatableComponent> _updatableComponents;
        private GraphicsDevice _graphicsDevice;
        private IRobbyTheRobot _fakeRobbyToGenerateTestGrid;
        private bool _isAddingComponents;

        private IsoGrid _grid;
        private HUD Hud { get; set; }
        private int TotalGenomes { get; set; }
        private int IndexGenomes { get; set; }
        private GenomeResult[] GenomeResults { get; set; }
        private RobbyPlayer RobbyPlayer { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="graphicsDevice">graphics device</param>
        /// <param name="gridBlockTexture">texture for a single grid block</param>
        /// <param name="sodaCanTexture">Texture of the soda can</param>
        /// <param name="robbyRobotTexture">Robby's texture</param>
        /// <param name="hud">array of grid contents</param>
        public SimulationManager(GraphicsDevice graphicsDevice, Texture2D gridBlockTexture, Texture2D sodaCanTexture, Texture2D robbyRobotLeftTexture, Texture2D robbyRobotRightTexture, SoundEffect pickupSound, HUD hud)
        {
            Hud = hud;
            _rand = new Random();
            _fakeRobbyToGenerateTestGrid = Robby.CreateRobby(0, 0, GridSize, 0, 0, _rand.Next(), 0);
            _graphicsDevice = graphicsDevice;
            _robbyRobotLeftTexture = robbyRobotLeftTexture;
            _robbyRobotRightTexture = robbyRobotRightTexture;
            _pickupSound = pickupSound;
            _grid = new IsoGrid(gridBlockTexture, sodaCanTexture, _fakeRobbyToGenerateTestGrid.GenerateRandomTestGrid());
            _grid.Position = new Vector2(_graphicsDevice.Viewport.Width / 2, (float)(_graphicsDevice.Viewport.Height / 2 - _grid.Height / 2));

            _drawableComponents = new List<IDrawableComponent>(){
        _grid
      };
            _updatableComponents = new List<IUpdatableComponent>(){
        _grid
      };

            _isAddingComponents = false;

        }

        /// <summary>
        /// Start the simulation 
        /// create robby and initialize it, add it to the grid, initialise and show infopane
        /// </summary>
        /// <param name="genomeResults">genome results from file/param>
        public void StartSimulation(GenomeResult[] genomeResults)
        {
            GenomeResults = genomeResults;
            IndexGenomes = 0;
            TotalGenomes = GenomeResults.Length;
            RobbyPlayer = new RobbyPlayer(_robbyRobotLeftTexture, _robbyRobotRightTexture, _pickupSound, GenomeResults[IndexGenomes].MovesToTake);

            SetupRobbySpeedButton();
            InitSimulation();

            Hud.InfoPane.Show = true;


            RobbyPlayer.OnScore += (double score) =>
            {
                Hud.InfoPane.CurrentScore += (int)score;
                Hud.InfoPane.CurrentMove = RobbyPlayer.CurrMove;
            };

            RobbyPlayer.OnMovesFinished += MovesFinishedHandler;
            // on finished change robby the robot again

            _grid.RobbyPlayer = RobbyPlayer;

        }

        private void SetupRobbySpeedButton()
        {
            Hud.RobbySpeedBtn.Text = "x1";
            Hud.RobbySpeedBtn.Position = new Vector2(
              _grid.Position.X + _grid.Width / 2 - Hud.RobbySpeedBtn.Rectangle.Width,
              _grid.Position.Y + _grid.Height - Hud.RobbySpeedBtn.Rectangle.Height
            );
            int count = 1;
            Hud.RobbySpeedBtn.Click += (sender, e) =>
            {
                if (count == 4)
                {
                    Hud.RobbySpeedBtn.Text = "Ludicrous";
                    RobbyPlayer.MovementInterval = 0;
                    count = 0;
                }
                else
                {
                    int factor = (int)Math.Pow(2, count);
                    Hud.RobbySpeedBtn.Text = "x" + factor;
                    RobbyPlayer.MovementInterval = 1000 / factor;
                    count++;
                }
            };
            Hud.RobbySpeedBtn.Show = true;
        }

        /// <summary>
        /// event handler for when a robby finishes its moves
        /// </summary>
        private void MovesFinishedHandler(object sender, EventArgs e)
        {
            IndexGenomes++;
            InitSimulation();
        }

        /// <summary>
        /// initialize robbyplayer, the grid, and infopane with next gnomeresults
        /// </summary>
        private void InitSimulation()
        {
            if (IndexGenomes >= TotalGenomes)
            {
                RobbyPlayer.OnMovesFinished -= MovesFinishedHandler;
                Hud.ShowMessage("Simulation Finished", Color.Black, Color.White);
                Hud.RobbySpeedBtn.Show = false;
                return;
            }
            var gridContents = _fakeRobbyToGenerateTestGrid.GenerateRandomTestGrid();
            // I need to do this because GridContents[] is a reference type so if I do not update both it will be changed by the other
            _grid.GridContents = gridContents;
            RobbyPlayer.GridContents = gridContents;

            RobbyPlayer.Genome = GenomeResults[IndexGenomes].MovesToTake;
            RobbyPlayer.RobbyCoordinates = new Vector2(_rand.Next(9), _rand.Next(9));
            RobbyPlayer.MaxMoves = GenomeResults[IndexGenomes].NumMoves;

            RobbyPlayer.CurrMove = 0;
            Hud.InfoPane.CurrentMove = 0;
            Hud.InfoPane.CurrentScore = 0;

            Hud.InfoPane.MaxMoves = GenomeResults[IndexGenomes].NumMoves;
            Hud.InfoPane.MaxScore = GenomeResults[IndexGenomes].MaxScore;
            Hud.InfoPane.GenerationNum = GenomeResults[IndexGenomes].GenerationNum;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // if you want to add components while draw is running, this is how you do it
            // useless for now
            if (!_isAddingComponents)
            {
                foreach (var component in _drawableComponents)
                {
                    component.Draw(gameTime, spriteBatch);
                }
            }

        }

        public void Update(GameTime gameTime)
        {
            if (!_isAddingComponents)
            {
                foreach (var component in _updatableComponents)
                {
                    component.Update(gameTime);
                }
            }
        }
    }

}