using System;

namespace RobbyTheRobot
{
    public class RobbyHelper
    {
        /// <summary>
        /// Moves robby and scores him based on the list of possible moves.
        /// </summary>
        /// <param name="moves">The list of moves</param>
        /// <param name="grid">The test grid to move robby on</param>
        /// <param name="rng">A random number generator</param>
        /// <param name="x">A start x position that is updated to his subsequent position</param>
        /// <param name="y">A start y position that is updated to his subsequent position</param>
        /// <returns>The score</returns>
        public static double ScoreForAllele(int[] moves, ContentsOfGrid[,] grid, Random rng, ref int x, ref int y)
        {
            DirectionOfGridContents direction = RobbyHelper.LookAround(x, y, grid);
            //find the gene
            int gene = RobbyHelper.FindGeneIndex(direction);
            //find the move
            var move = moves[gene];
            bool done;
            do
            {
                done = true;
                switch (move)
                {
                    case 0://move north
                        if (direction.N == ContentsOfGrid.Wall)
                            return -5;
                        y -= 1;
                        break;
                    case 1://move south
                        if (direction.S == ContentsOfGrid.Wall)
                            return -5;
                        y += 1;
                        break;
                    case 2: //move east
                        if (direction.E == ContentsOfGrid.Wall)
                            return -5;
                        x += 1;
                        break;
                    case 3: //move west
                        if (direction.W == ContentsOfGrid.Wall)
                            return -5;
                        x -= 1;
                        break;
                    case 4: //do nothong
                        return 0;
                    case 5: //pick up can
                        if (direction.Current == ContentsOfGrid.Can) //there is a can
                        {
                            grid[x, y] = ContentsOfGrid.Empty;
                            return +10;
                        }
                        else
                            return -1; //penalty for picking up nothing
                    case 6: //random move
                        done = false;
                        move = rng.Next(0, 6);
                        break;
                }
            }
            while (!done);
            return 0;
        }

        /// <summary>
        /// Used to fill up a DirectionsContent struct based on Robby's position in the 
        /// grid and what is immediately adjacent to him.
        /// </summary>
        /// <param name="x">Robby's x coordinates</param>
        /// <param name="y">Robby's y coordinates</param>
        /// <param name="grid">The test grid where Robby is</param>
        /// <returns>What Robby sees in all directions plus current</returns>
        private static DirectionOfGridContents LookAround(int x, int y, ContentsOfGrid[,] grid)
        {
            return new DirectionOfGridContents
            {
                N = (y == 0) ? ContentsOfGrid.Wall : grid[x, y - 1],
                S = (y == grid.GetLength(1) - 1) ? ContentsOfGrid.Wall : grid[x, y + 1],
                E = (x == grid.GetLength(0) - 1) ? ContentsOfGrid.Wall : grid[x + 1, y],
                W = (x == 0) ? ContentsOfGrid.Wall : grid[x - 1, y],
                Current = grid[x, y],
            };
        }

        /// <summary>
        /// Provides the index of the gene for the given set of grid directions
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        private static int FindGeneIndex(DirectionOfGridContents dir)
        {

            return getIndexForDirection(dir.N, 4)
                + getIndexForDirection(dir.S, 3)
                + getIndexForDirection(dir.E, 2)
                + getIndexForDirection(dir.W, 1)
                + getIndexForDirection(dir.Current, 0);
        }
        private static int CheapPow(int value, int exp)
        {
            if (exp == 0) return 1;
            int res = value;
            while (exp-- > 1)
                res *= value;
            return res;
        }

        /// <summary>
        /// Used to build up the index of the gene in the Chromosome
        /// </summary>
        /// <param name="content">Content in a given direction</param>
        /// <param name="power">Exponent of 10</param>
        /// <returns>Partial calculation of the gene's index</returns>
        private static int getIndexForDirection(ContentsOfGrid content, int power)
        {
            if (content == ContentsOfGrid.Empty)
                return 0;
            if (content == ContentsOfGrid.Can)
                return (CheapPow(3, power));
            else
                return (2 * CheapPow(3, power));
        }
    }
}