﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm;
using FileIO;
using System.Diagnostics;
namespace RobbyTheRobot
{
    internal class RobbyTheRobot : IRobbyTheRobot
    {
        public int NumberOfActions { get; private set; }
        public int NumberOfTestGrids { get; private set; }
        public int GridSize { get; private set; }
        public double MutationRate { get; private set; }
        public double EliteRate { get; private set; }
        public int PopulationSize { get; private set; }
        public int NumberOfGenerations { get; private set; }
        public event GenerationDone FileWritten;

        internal RobbyTheRobot(
            int numberOfActions,
            int numberOfTestGrids,
            int gridSize,
            int numberOfGenerations,
            double mutationRate,
            double eliteRate,
            int populationSize
        )
        {
            this.NumberOfActions = numberOfActions;
            this.NumberOfActions = numberOfActions;
            this.NumberOfTestGrids = numberOfTestGrids;
            this.GridSize = gridSize;
            this.NumberOfGenerations = numberOfGenerations;
            this.MutationRate = mutationRate;
            this.EliteRate = eliteRate;
            this.PopulationSize = populationSize;
        }

        private static Random Rand = new Random();

        public ContentsOfGrid[,] GenerateRandomTestGrid()
        {
            // int seed = 0;
            var grid = new ContentsOfGrid[this.GridSize, this.GridSize];
            var rand = new Random();
            int count = this.GridSize * this.GridSize / 2;

            while (count > 0)
            {
                int x = rand.Next(this.GridSize);
                int y = rand.Next(this.GridSize);
                if (grid[x, y] != ContentsOfGrid.Can)
                {
                    grid[x, y] = ContentsOfGrid.Can;
                    count--;
                }
            }

            return grid;
        }

        private double PerformFitnessCalculation(IChromosome chromosome, IGeneration generation)
        {
            int x = Rand.Next(GridSize);
            int y = Rand.Next(GridSize);
            var rand = new Random();
            var grid = this.GenerateRandomTestGrid();

            double score = 0;

            for (int i = 0; i < this.NumberOfActions; i++)
            {
                score += RobbyHelper.ScoreForAllele(chromosome.Genes, grid, rand, ref x, ref y);
            }
            return score;
        }


        /// Generates a series of possible solutions based on the generations and saves them to disk.
        /// The text files generated must contain a comma seperated list of the max score, number 
        /// of moves to display in the gui and all the actions robby will take (i.e the genes in the Chromosome).
        /// The top candidate of the 1st, 20th, 100, 200, 500 and 1000th generation will be saved.

        public void GeneratePossibleSolutions(string folderPath)
        {
            if (!System.IO.Directory.Exists(folderPath))
            {
                throw new System.IO.DirectoryNotFoundException();
            }

            IGeneticAlgorithm algorithm = GeneticLib.CreateGeneticAlgorithm(
               populationSize: PopulationSize,
               numberOfGenes: 243,
               lengthOfGene: 7,
               mutationRate: MutationRate,
               eliteRate: EliteRate,
               numberOfTrials: NumberOfTestGrids,
               fitnessCalculation: PerformFitnessCalculation
           );

            int[] toSave = { 1, 20, 100, 200, 500, 1000 };
            List<GenomeResult> results = new List<GenomeResult>();

            var timer = Stopwatch.StartNew();
            var maxScore = 10 * GridSize * GridSize / 2;
            for (int i = 0; i < NumberOfGenerations; i++)
            {
                var sw = Stopwatch.StartNew();
                IGeneration generation = algorithm.GenerateGeneration();
                sw.Stop();

                FileWritten?.Invoke(i + 1, generation, sw.ElapsedMilliseconds);
                if (toSave.Contains(i + 1)) 
                {
                    var bestChromosome = generation[0];
                    for(int c=0;c<generation.NumberOfChromosomes;c++){
                        if(generation[c].Fitness == generation.MaxFitness){
                            bestChromosome = generation[c];
                            break;
                        }
                    }
                    results.Add(new GenomeResult(i + 1, maxScore, NumberOfActions, bestChromosome.Genes));
                }
            }
            timer.Stop();
            Console.WriteLine("Total took: {0}ms", timer.ElapsedMilliseconds);

            PossibleSolutionsFileIO.SaveGenomeResults(folderPath, results.ToArray());
        }
    }
}