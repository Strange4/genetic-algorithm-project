﻿using System.IO;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Takes care of reading and writing the results
    /// each result is a file
    /// file should be 1 line and look like this: <maxScore>,<nbMoves>,<comma seperated movesToTake>
    /// </summary>
    public class PossibleSolutionsFileIO
    {
        private static readonly int NumOfPossibleMoves = 7;
        // NumOfPossibleSituations (243) + 2 for maxMoves and nbMoves 
        private static readonly int NumOfCommas = 245;

        /// <summary>
        /// Gets genome results from folder of appropriately formated txt files
        /// </summary>
        /// <param name="folderPath">path of the folder with files representing genome results</param>
        /// <param name="genomeResults"> OUT param: Array of GenomeResult</param>
        /// <returns>bool: isValid or not</returns>
        public static GenomeResult[] GetGenomeResults(string folderPath)
        {
            if (!Directory.Exists(folderPath))
                throw new ArgumentException("Directory does not exist");

            Regex reg = new Regex(@"\\[0-9]+\.txt$");
            string[] files = Directory.EnumerateFiles(folderPath).Where(path => reg.IsMatch(path)).ToArray();
            if (files.Length == 0)
                throw new ArgumentException("Folder does not contain any (digit(s)).txt file");

            GenomeResult[] genomeResults = new GenomeResult[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                string generationStr = files[i].Split(@"\").Last().Split(".").First();
                int generationNum;
                if (!int.TryParse(generationStr, out generationNum))
                    throw new ArgumentException("File name must be (digit(s)).txt where the digits represent the generation number.");

                // divide maxScore, nbMoves, movesToTake from the file
                string contents = File.ReadAllLines(files[i])[0];
                string[] results = contents.Split(",");
                if (results.Length != NumOfCommas)
                    throw new ArgumentException("Not right number of commas in the first line. It should be: " + NumOfCommas);

                string[] strGenome = results.Skip(2).ToArray();

                // get maxScore, nbMoves as int
                int maxScore, nbMoves;
                if (!int.TryParse(results[0], out maxScore) || !int.TryParse(results[1], out nbMoves))
                    throw new ArgumentException("First 2 values (maxScore and nbMoves) are not of type int");

                // get genome as int array
                int[] numGenome = new int[strGenome.Length];
                for (int j = 0; j < strGenome.Length; j++)
                {
                    if (!int.TryParse(strGenome[j], out numGenome[j]))
                        throw new ArgumentException("Gene value is not of type int");
                    if (numGenome[j] < 0 || numGenome[j] >= NumOfPossibleMoves)
                        throw new ArgumentException("Gene value is not in range 0-" + (NumOfPossibleMoves - 1));
                }

                // get genome as PossibleMoves array
                int[] movesToTake = new int[numGenome.Length];
                for (int j = 0; j < numGenome.Length; j++)
                {
                    movesToTake[j] = numGenome[j];
                }

                genomeResults[i] = new GenomeResult(generationNum, maxScore, nbMoves, movesToTake);

            }

            Array.Sort(genomeResults);

            return genomeResults;
        }

        /// <summary>
        /// Writes genome results to a folder
        /// </summary>
        /// <param name="folderPath">path of the folder with files representing genome results</param>
        /// <param name="genomeResults"> OUT param: Array of GenomeResult</param>
        /// <returns>bool: isValid or not</returns>
        public static void SaveGenomeResults(string folderPath, GenomeResult[] results)
        {
            if (!Directory.Exists(folderPath))
                throw new ArgumentException("Directory does not exist");

            foreach (var result in results)
            {
                string fileName = String.Format("{0}/{1}.txt", folderPath, result.GenerationNum);
                string output = String.Format("{0},{1},{2}", result.MaxScore, result.NumMoves, string.Join(",", result.MovesToTake));
                File.WriteAllText(fileName, output);
            }
        }
    }
}
