using System;

namespace FileIO
{
    /// <summary>
    /// Object storing:
    /// Max Score robby can possibly ever get,
    /// number of moves to visualize, 
    /// and PossibleMoves[] that represents the genome
    /// </summary>
    public class GenomeResult : IComparable<GenomeResult>
    {
        public GenomeResult(int generationNum, int maxScore, int numMoves, int[] movesToTake)
        {
            GenerationNum = generationNum;
            MaxScore = maxScore;
            NumMoves = numMoves;
            MovesToTake = movesToTake;
        }
        public int GenerationNum { get; private set; }
        public int MaxScore { get; private set; }
        public int NumMoves { get; private set; }
        public int[] MovesToTake { get; set; }

        public int CompareTo(GenomeResult other)
        {
            return Math.Sign(this.GenerationNum - other.GenerationNum);
        }

    }
}