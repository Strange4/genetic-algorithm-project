using System;
using System.Threading.Tasks;
namespace GeneticAlgorithm
{
    internal class Generation : IGenerationDetails
    {
        private readonly IChromosome[] chromosomes;
        private readonly Random random;
        private readonly IGeneticAlgorithm geneticAlgorithm;

        // Creates a new random generation from scratch using the genetic algorihm
        public Generation(IGeneticAlgorithm geneticAlgorithm, int? seed = null)
        {
            this.geneticAlgorithm = geneticAlgorithm;
            random = seed == null ? new Random() : new Random(seed.Value);
            chromosomes = new Chromosome[geneticAlgorithm.PopulationSize];
            GenerateRandomGen();
            this.EvaluateFitnessOfPopulation();
        }

        private void GenerateRandomGen()
        {
            for (int i = 0; i < geneticAlgorithm.PopulationSize; i++)
            {
                chromosomes[i] = new Chromosome(geneticAlgorithm.NumberOfGenes, geneticAlgorithm.LengthOfGene);
            }
        }

        public Generation(IChromosome[] chromosomes, IGeneticAlgorithm geneticAlgorithm, int? seed = null)
        {
            random = seed == null ? new Random() : new Random(seed.Value);
            this.geneticAlgorithm = geneticAlgorithm;
            this.chromosomes = chromosomes;
            Array.Copy(chromosomes, this.chromosomes, chromosomes.Length);
            this.EvaluateFitnessOfPopulation();
        }

        public double AverageFitness
        {
            get;
            private set;
        }

        public double MaxFitness
        {
            get;
            private set;
        }

        public long NumberOfChromosomes
        {
            get => chromosomes.Length;
        }

        public IChromosome this[int index]
        {
            get => chromosomes[index];
        }

        /**
            selecting the parent proportional to fitness

            the more fitness you have the more likely you are to be selected
        **/
        public IChromosome SelectParent()
        {
            return BestOf(10);
        }


        private IChromosome BestOf(int count)
        {
            var rand = new Random();
            IChromosome best = chromosomes[rand.Next(chromosomes.Length)];
            for (int i = 0; i < count; i++)
            {
                var randomChromosome = chromosomes[rand.Next(chromosomes.Length)];
                if (randomChromosome.Fitness > best.Fitness)
                {
                    best = randomChromosome;
                }
            }
            return best;
        }

        public void EvaluateFitnessOfPopulation()
        {

            Parallel.ForEach(chromosomes, chromosome =>
            {
                Chromosome chrome = chromosome as Chromosome;

                double fitnessSum = 0;
                double fitnessMin = double.MaxValue;
                for (int i = 0; i < geneticAlgorithm.NumberOfTrials; i++)
                {
                    var fitness = geneticAlgorithm.FitnessCalculation(chrome, this);
                    fitnessMin = Math.Min(fitness, fitnessMin);
                    fitnessSum += fitness;
                }
                chrome.Fitness = fitnessSum / geneticAlgorithm.NumberOfTrials;


            });

            var max = double.MinValue;

            foreach (var chromosome in chromosomes)
            {
                if (chromosome.Fitness > max)
                {
                    max = chromosome.Fitness;
                }
            }


            MaxFitness = max;
        }
    }
}