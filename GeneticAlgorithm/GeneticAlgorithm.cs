using System;

namespace GeneticAlgorithm
{
    internal class GeneticAlgorithm : IGeneticAlgorithm
    {
        public int PopulationSize { get; private set; }
        public int NumberOfGenes { get; private set; }
        public int LengthOfGene { get; private set; }
        public double MutationRate { get; private set; }
        public double EliteRate { get; private set; }
        public int NumberOfTrials { get; private set; }
        public long GenerationCount { get; private set; }
        public IGeneration CurrentGeneration { get; private set; }
        public FitnessEventHandler FitnessCalculation { get; private set; }
        private readonly int? seed;

        public GeneticAlgorithm(
            int populationSize,
            int numberOfGenes,
            int lengthOfGene,
            double mutationRate,
            double eliteRate,
            int numberOfTrials,
            FitnessEventHandler fitnessCalculation,
            int? seed = null
        )
        {
            PopulationSize = populationSize;
            NumberOfGenes = numberOfGenes;
            LengthOfGene = lengthOfGene;
            MutationRate = mutationRate;
            EliteRate = eliteRate;
            NumberOfTrials = numberOfTrials;
            FitnessCalculation = fitnessCalculation;
            this.seed = seed;
        }

        public IGeneration GenerateGeneration()
        {
            GenerationCount++;

            if (CurrentGeneration == null)
                return CurrentGeneration = new Generation(this, seed);

            var thisGeneration = CurrentGeneration as IGenerationDetails;
            var newGeneration = new IChromosome[thisGeneration.NumberOfChromosomes];
            var numberOfElites = (int)(thisGeneration.NumberOfChromosomes * EliteRate);

            for (int i = 0; i < numberOfElites; i++)
            {
                newGeneration[i] = thisGeneration[i];
            }
            for (int i = numberOfElites; i < thisGeneration.NumberOfChromosomes;)
            {
                var parent1 = thisGeneration.SelectParent();
                var parent2 = thisGeneration.SelectParent();
                var children = parent1.Reproduce(parent2, MutationRate);
                newGeneration[i++] = children[0];
                newGeneration[i++] = children[1];
            }

            CurrentGeneration = new Generation(newGeneration, this, seed);
            return CurrentGeneration;
        }

    }
}