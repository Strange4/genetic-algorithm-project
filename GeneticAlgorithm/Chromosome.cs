
using System;

namespace GeneticAlgorithm
{
    internal class Chromosome : IChromosome
    {
        private readonly int numberOfGenePossibilities;
        private readonly int? seed;

        public Chromosome(int chromosomeLength, int numberOfGenePossibilities, int? seed = null)
        {
            Genes = new int[chromosomeLength];
            var random = seed == null ? new Random() : new Random(seed.Value);
            for (int i = 0; i < Genes.Length; i++)
            {
                Genes[i] = random.Next(0, numberOfGenePossibilities);
            }
            this.numberOfGenePossibilities = numberOfGenePossibilities;
            this.seed = seed;
            ReproductionMethod = DefaultReproductionMethod;
        }

        public Chromosome(IChromosome other, int? seed)
        {
            Genes = new int[other.Length];
            Array.Copy(other.Genes, Genes, other.Genes.Length);
            numberOfGenePossibilities = other.Length;
            this.seed = seed;
            ReproductionMethod = DefaultReproductionMethod;
        }

        public Reproduce ReproductionMethod
        {
            get; set;
        }

        public int[] Genes
        {
            get;
        }

        public IChromosome[] Reproduce(IChromosome spouse, double mutationProb)
        {
            return ReproductionMethod(this, spouse, mutationProb, seed);
        }

        public int this[int index]
        {
            get => Genes[index];
        }


        public double Fitness { get; set; }

        public int Length => Genes.Length;

        public int CompareTo(IChromosome other)
        {
            return Math.Sign(Fitness - other.Fitness);
        }

        private static void MutateChromosome(IChromosome chromosome, double mutationProb, Random rand)
        {
            var genes = chromosome.Genes;
            for (int i = 0; i < genes.Length; i++)
            {
                if (rand.NextDouble() > 1 - mutationProb)
                {
                    genes[i] = rand.Next(7);
                }
            }
        }

        private IChromosome[] DefaultReproductionMethod(IChromosome a, IChromosome b, double mutationProb, int? seed = null)
        {
            var random = seed == null ? new Random() : new Random(seed.Value);
            var chromosomeLength = a.Genes.Length;
            var point1 = random.Next(0, chromosomeLength);
            var point2 = random.Next(0, chromosomeLength);

            if (point1 > point2)
            {
                (point1, point2) = (point2, point1);
            }

            // reproducing and mutating the first child
            var firstChild = new Chromosome(a, seed + 1) { ReproductionMethod = a.ReproductionMethod };
            Array.Copy(b.Genes, point1, firstChild.Genes, point1, point2 - point1);
            MutateChromosome(firstChild, mutationProb, random);


            // reproducting and mutating the second child
            var secondChild = new Chromosome(b, seed + 2) { ReproductionMethod = b.ReproductionMethod };
            Array.Copy(a.Genes, point1, secondChild.Genes, point1, point2 - point1);
            MutateChromosome(secondChild, mutationProb, random);

            return new IChromosome[] { firstChild, secondChild };
        }
    }
}