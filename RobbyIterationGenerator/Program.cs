﻿using System;
using RobbyTheRobot;
using Sharprompt;
using Sharprompt.Fluent;
using System.ComponentModel.DataAnnotations;
using System.IO;
namespace RobbyIterationGenerator
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(@"
            
██████╗  ██████╗ ██████╗ ██████╗ ██╗   ██╗     ██████╗ ███████╗███╗   ██╗███████╗██████╗  █████╗ ████████╗ ██████╗ ██████╗ ██╗
██╔══██╗██╔═══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝    ██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗██║
██████╔╝██║   ██║██████╔╝██████╔╝ ╚████╔╝     ██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝███████║   ██║   ██║   ██║██████╔╝██║
██╔══██╗██║   ██║██╔══██╗██╔══██╗  ╚██╔╝      ██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║   ██║   ██║   ██║██╔══██╗╚═╝
██║  ██║╚██████╔╝██████╔╝██████╔╝   ██║       ╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║  ██║   ██║   ╚██████╔╝██║  ██║██╗
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚═════╝    ╚═╝        ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝
                                                                                                                              
");
            var numberOfActions = Prompt.Input<int>(
                o => o.WithMessage("Number of actions?")
                    .WithDefaultValue(200)
                    .AddValidators(new[] { MinValue(1) }));

            var numberOfTestGrids = Prompt.Input<int>(
                o => o.WithMessage("Number of test grids?")
                    .WithDefaultValue(100)
                    .AddValidators(new[] { MinValue(1) }));

            var gridSize = Prompt.Select<int>(
                o => o.WithMessage("Grid size?")
                    .WithDefaultValue(10)
                    .WithItems(new[] { 10, 20, 50 }));

            var numberOfGenerations = Prompt.Input<int>(
                o => o.WithMessage("Number of generations?")
                    .WithDefaultValue(1000)
                    .AddValidators(new[] { MinValue(1) }));

            var mutationRate = Prompt.Select<double>(
                o => o.WithMessage("Mutation rate?")
                    .WithDefaultValue(0.01)
                    .WithItems(new[] { 0.01, 0.03, 0.05, 0.1 }));

            var eliteRate = Prompt.Select<double>(
                o => o.WithMessage("Elite rate?")
                    .WithDefaultValue(0.1)
                    .WithItems(new[] { 0.05, 0.1, 0.15, 0.2 }));

            var populationSize = Prompt.Input<int>(
               o => o.WithMessage("Size of population?")
                   .WithDefaultValue(200)
                   .AddValidators(new[] { MinValue(1) }));

            var outputDirectory = Prompt.Input<string>(
                o => o.WithMessage("Output Directory?")
                    .WithDefaultValue("../RobbyOutput")
                    .AddValidators(new []{ ValidFolder() }));

            var robby = Robby.CreateRobby(
                numberOfActions: numberOfActions,
                numberOfTestGrids: numberOfTestGrids,
                gridSize: gridSize,
                numberOfGenerations: numberOfGenerations,
                mutationRate: mutationRate,
                eliteRate: eliteRate,
                populationSize: populationSize
            );
            Console.WriteLine("Robby Made!");
            robby.FileWritten += (count, generation, timeTaken) =>
            {
                Console.WriteLine($"Generation: {count}, Max Fitness: {generation.MaxFitness}, time: {timeTaken}ms");
            };
            robby.GeneratePossibleSolutions(outputDirectory);
            Console.WriteLine("Done running");
        }
        public static Func<object, ValidationResult> ValidFolder()
        {
            return input =>
            {
                if (!(input is string inputString))
                {
                    return new ValidationResult("The input should be a string");
                }
                if (Directory.Exists(inputString))
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult($"The directory doesn't exist");
            };
        }

        public static Func<object, ValidationResult> MinValue(int value)
        {
            return input =>
            {
                if (!(input is int))
                {
                    return new ValidationResult("The input should be an integer");
                }
                var intInput = (int)input;
                if (intInput >= value)
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult($"The number should be at least {value}");
            };
        }
    }


}
